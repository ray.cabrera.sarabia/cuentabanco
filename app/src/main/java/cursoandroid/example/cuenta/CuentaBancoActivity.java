package cursoandroid.example.cuenta;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText cuentaEditText;
    private EditText nombreEditText;
    private EditText bancoEditText;
    private EditText saldoEditText;
    private Button enviarButton;
    private Button salirButton;
    private double saldo = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        cuentaEditText = findViewById(R.id.lblCuenta);
        nombreEditText = findViewById(R.id.lblNombre);
        bancoEditText = findViewById(R.id.lblBanco);
        saldoEditText = findViewById(R.id.lblSaldo);
        enviarButton = findViewById(R.id.btnEnviar);
        salirButton = findViewById(R.id.btnSalir);


        enviarButton.setOnClickListener(new View.OnClickListener() {
            @Override

                    public void onClick(View v) {
                realizarDeposito();
            }
        });





        salirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarRetiro();
            }
        });
    }

    private void realizarDeposito() {
        String saldoStr = saldoEditText.getText().toString();
        if (!saldoStr.isEmpty()) {
            double monto = Double.parseDouble(saldoStr);
            saldo += monto;
            actualizarSaldo();
        } else {
            mostrarMensaje("Ingresa un monto válido para el depósito.");
        }
    }

    private void realizarRetiro() {
        String saldoStr = saldoEditText.getText().toString();
        if (!saldoStr.isEmpty()) {
            double monto = Double.parseDouble(saldoStr);
            if (monto <= saldo) {
                saldo -= monto;
                actualizarSaldo();
            } else {
                mostrarMensaje("No tienes suficiente saldo para el retiro.");
            }
        } else {
            mostrarMensaje("Ingresa un monto válido para el retiro.");
        }
    }

    private void actualizarSaldo() {
        saldoEditText.setText(String.valueOf(saldo));
    }

    private void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
