package cursoandroid.example.cuenta;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText edtCuenta;
    private EditText edtNombre;
    private EditText edtBanco;
    private EditText edtSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        edtCuenta = findViewById(R.id.lblCuenta);
        edtNombre = findViewById(R.id.lblNombre);
        edtBanco = findViewById(R.id.lblBanco);
        edtSaldo = findViewById(R.id.lblSaldo);

        Button btnEnviar = findViewById(R.id.btnEnviar);
        Button btnSalir = findViewById(R.id.btnSalir);


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtén el texto de los campos de entrada
                String cuenta = edtCuenta.getText().toString();
                String nombre = edtNombre.getText().toString();
                String banco = edtBanco.getText().toString();
                String saldo = edtSaldo.getText().toString();


                Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
                // Pasa datos a la actividad CuentaBancoActivity
                intent.putExtra("cuenta", cuenta);
                intent.putExtra("nombre", nombre);
                intent.putExtra("banco", banco);
                intent.putExtra("saldo", saldo);


                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
